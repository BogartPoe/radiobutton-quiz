﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using static Android.Views.View;
using Android.Views;

namespace App4
{
    [Activity(Label = "Quiz Game", Theme = "@style/Theme.AppCompat.Light.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, IOnClickListener
    {
        public Button btnstart, btnexit;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            btnstart = FindViewById<Button>(Resource.Id.btn_start);
            btnexit = FindViewById<Button>(Resource.Id.btn_exit);

            btnstart.SetOnClickListener(this);
            btnexit.SetOnClickListener(this);


        }



        public void OnClick(View v)
        {
            //action when login button is click
            if (v.Id == Resource.Id.btn_start)
            {

                StartActivity(new Android.Content.Intent(this, typeof(question1)));
                Toast.MakeText(this, "Quiz Start", ToastLength.Short).Show();
                Toast.MakeText(this, "Question 1", ToastLength.Short).Show();
                Finish();
            }
            //action when forgotpassword button is click
            else if (v.Id == Resource.Id.btn_exit)
            {

                //closing the app
                Finish();
                Toast.MakeText(this, "Thank You!", ToastLength.Short).Show();
            }
        }

        
    }
}

